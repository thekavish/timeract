import React from 'react'
import './App.css'

function App () {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          <code>TIMER</code>
        </p>
        <Timer/>
      </header>
    </div>
  )
}

class Clock extends React.Component {
  render () {
    const times = []
    this.props.times.forEach((time, index) => {
      let s = String(time)
      while (s.length < 2) { s = '0' + s }

      if (index !== 0) { times.push('\u00A0:\u00A0') }
      times.push(<td key={index}>{s}</td>)
    })

    return (
      <button className="btn clock">
        <tr>{times}</tr>
      </button>
    )
  }
}

class Timer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      milliseconds: 0,
      isPaused: true,
    }

    this.startTimer = this.startTimer.bind(this)
    this.stopTimer = this.stopTimer.bind(this)
  }

  handleTimer () {
    if (!this.state.isPaused) {
      this.stopTimer()
    }
    else {
      this.startTimer()
    }

    this.setState({
      isPaused: !this.state.isPaused,
    })
  }

  resetTimer () {
    this.setState({
      milliseconds: 0,
      isPaused: true,
    })
  }

  stopTimer () {
    clearInterval(this.timer)
  }

  startTimer () {
    this.timer = setInterval(() => this.setState({
      milliseconds: this.state.milliseconds + 1,
    }), 10)
  }

  render () {
    const psd = this.state.isPaused,
      ms = this.state.milliseconds,
      timerValue = parseMilliseconds(ms)

    let clockStatus = 'Start'
    if (ms) {
      clockStatus = psd ? 'Resume' : 'Pause'
    }

    return (
      <div className="timer">
        <button
          className={'btn reset ' + (ms && psd ? 'error' : 'default')}
          disabled={(ms && psd) ? '' : 'disabled'}
          onClick={() => this.resetTimer()}
        >Reset
        </button>
        <Clock
          times={timerValue}
        />
        <button
          className={'btn ' + (clockStatus === 'Pause' ? 'error' : 'success')}
          onClick={() => this.handleTimer()}
        >
          {clockStatus}
        </button>
      </div>
    )
  }
}

function parseMilliseconds (time) {
  let timeElapsed = time

  const ms = timeElapsed % 100
  timeElapsed = Math.floor(timeElapsed / 100)

  const sec = timeElapsed % 60
  timeElapsed = Math.floor(timeElapsed / 60)

  const min = timeElapsed % 60

  return [min, sec, ms]
  // return (min.pad() + ' : ' + sec.pad() + ' : ' + ms.pad())

  /*const minutes = Math.floor(timeElapsed / 6000)
  timeElapsed = timeElapsed % 6000

  const seconds = Math.floor(timeElapsed / 100)
  timeElapsed = timeElapsed % 100

  const miliseconds = timeElapsed

  return (minutes + ':' + seconds + ':' + miliseconds)*/
}

export default App
